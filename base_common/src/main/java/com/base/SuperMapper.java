package com.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @title  Mybatis mapper 父类，注意这个类不要让 MyBatisPlus 扫描到！！
 * @Author 覃球球
 * @Version 1.0 on 2017/12/11.
 * @Copyright 长笛龙吟
 */
public interface  SuperMapper<T> extends BaseMapper<T> {

}
