package com.beone.generator;

/**
 * @title
 * @Author 覃球球
 * @Version 1.0 on 2018/11/1.
 * @Copyright 贝旺科技
 */
public class FormLabelInfo {

    private String text;  //显示label

    private String value; //控件值

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
