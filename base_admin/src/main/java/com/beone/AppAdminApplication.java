package com.beone;

import com.beone.admin.filter.GloableFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AppAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppAdminApplication.class, args);
	}

	/**
	 * 获取每次请求的参数，便于日志跟踪
	 * @return
	 */
	@Bean
	public FilterRegistrationBean paramsLogFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new GloableFilter());
		registration.addUrlPatterns("/system/*");
		registration.setName("paramsLogFilter");
		registration.setOrder(3);
		return registration;
	}
}
