package com.beone.admin.config.druid;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @title  Druid 连接池配置
 * @Author 覃球球
 * @Version 1.0 on 2017/12/6.
 * @Copyright 长笛龙吟
 */
@Configuration
public class DruidDataSourceConfig {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${druid.type}")
    private Class<? extends DataSource> dataSourceType;


    /**
     * Master 数据库连接池对象初始化
     * @return
     */
    @Bean(name = "masterDataSource", destroyMethod = "close", initMethod = "init")
    @Primary
    @ConfigurationProperties(prefix = "druid.master")
    public DataSource masterDruidDataSource(){
        logger.info("druid master init..................");
        return DataSourceBuilder.create().type(dataSourceType).build();
    }

    /**
     * Slave 数据库连接池对象初始化
     * @return

    @Bean(name = "slaveDataSource", destroyMethod = "close", initMethod = "init")
    @ConfigurationProperties(prefix = "druid.slave")
    public DataSource slaveDruidDataSource(){
        logger.info("druid slave init..................");
        return DataSourceBuilder.create().type(dataSourceType).build();
    }*/
}
