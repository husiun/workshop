package com.beone.admin.config.mybatis;

import com.base.common.datasource.DbContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @title   程序运行时将数据源切换绑定到ThreadLocal对象上
 * @Author 覃球球
 * @Version 1.0 on 2017/12/6.
 * @Copyright 长笛龙吟
 */
public class ReadWriteSplitRoutingDataSource extends AbstractRoutingDataSource {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected Object determineCurrentLookupKey() {
        Object dataSourceKey = DbContextHolder.getDbType();
        logger.info("DataSource  Choice: {}",dataSourceKey);
        return dataSourceKey;
    }
}
