package com.beone.admin.service;

import com.beone.admin.entity.SysJob;
import com.base.ISuperService;
import com.beone.admin.utils.PaginationGatagridTable;

/**
 * @Title  系统Job服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-25
 * @Copyright 贝旺科权
 */
public interface SysJobService extends ISuperService<SysJob> {
    /**
     * 分页显示后台任务列表
     * @param job
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    PaginationGatagridTable getSysJobPagination(SysJob job, int currPage, int pageSize);

    /**
     * 保存任务
     * @param sysJob
     * @return
     */
    boolean saveJob(SysJob sysJob);
}
