package com.beone.admin.service;

import com.alibaba.fastjson.JSONObject;
import com.base.ISuperService;
import com.beone.admin.entity.BasePermission;
import com.beone.admin.entity.BaseUser;
import com.beone.admin.utils.PaginationGatagridTable;

import java.util.List;

/**
 * @Title 运维数据_系统用户信息表 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BaseUserService extends ISuperService<BaseUser> {

    /**
     * 根据用户名获取后台管理用户信息
     * @param name
     * @return
     */
    BaseUser loadUserByUsername(String name);

    /**
     * 根据用户ID，获取用户所有的URL权限列表
     * @param userId 用户Id
     * @param actionType  page  表示只显示页面权限   all  显示所有权限
     * @return
     */
    List<BasePermission> queryPermissions(String actionType, Integer userId);

    /**
     * 根据用户Id, 修改用户密码
     * @return
     */
    JSONObject updatePwd(String oldPassword, String newPassword1, Integer userId);

    /**
     * 分页显示后台用户列表
     * @param user
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    PaginationGatagridTable getUserPagination(BaseUser user, int currPage, int pageSize);

    /**
     * 是否supervisor用户
     * @param userId
     * @return  true 是   false 否
     */
    boolean isSupervisor(Integer userId);

    /**
     * 保存用户信息
     */
    boolean saveUser(BaseUser user);


    /**
     * 检查用户是否存在
     * @param userId
     * @param userAccount
     * @return  true  不存在   false 存在
     */
    boolean checkUserNotExist(Integer userId, String userAccount);
}
