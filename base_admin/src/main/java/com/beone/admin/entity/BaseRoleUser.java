package com.beone.admin.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title 运维数据_用户对应的角色 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@TableName("base_role_user")
public class BaseRoleUser extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 账户ID
     */
    @TableId("user_id")
	private Integer userId;
    /**
     * 角色ID
     */
	@TableField("role_id")
	private Integer roleId;


	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "BaseRoleUser{" +
			", userId=" + userId +
			", roleId=" + roleId +
			"}";
	}
}
